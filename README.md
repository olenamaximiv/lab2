Maksymiv Olena

IH.M.-22

BlogApp with:
Comment.feature
ForgotPassword.feature
Login.feature
Logout.feature
Post.feature
Profile.feature
Registration.feature
ResetPassword.feature


Technologies used:
Python
Javascript
Django
SQLite
PostgreSQL

To local setup:
	git clone
	
	your/path/to/pipenv --python /usr/bin/python3
	your/path/to/pipenv install --python 3.8
	your/path/to/pipenv install
	your/path/to/pipenv shell
	pip install -r requirements.txt
	python manage.py migrate
	python manage.py createsuperuser
	python manage.py runserver


You can view your local web-site here: http://127.0.0.1:8000/


To run Dockerfile:
	docker build -t my-django-app .
	
	docker run -p 8000:8000 my-django-app

View here: http://127.0.0.1:8000/
