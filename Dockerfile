# Use an official Python runtime as the base image
FROM python:3.8

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory in the container
WORKDIR /app

# Copy the Pipfile and Pipfile.lock to the working directory
COPY Pipfile Pipfile.lock ./

# Install Pipenv
RUN pip install pipenv

# Install project dependencies
RUN pipenv install --system --deploy --ignore-pipfile

# Copy the project files to the working directory
COPY . .

RUN pip install -r requirements.txt

# Run migrations
RUN python manage.py migrate

# Create a superuser (replace with your desired credentials)
RUN echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@example.com', 'admin123')" | python manage.py shell

# Expose the port on which the app will run (change if necessary)
EXPOSE 8000

# Run the Django development server
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

